################################################################################
# SaaS Config Management Data Fille
#   Author: SWV
#   Notes : This File Stores all the SaaS Configuration Management Data
#         : 
#
################################################################################

@{
    CafeMas = @{
        Commands = @(
    
        )
        Files = @(
            "/etc/hosts"
            "/opt/mcp/config/ha.yml"
            "/etc/asterisk/http.conf"
            "/etc/asterisk/indications.conf"
            "/etc/asterisk/logger.conf"
            "/etc/asterisk/manager.conf"
            "/etc/asterisk/rtp.conf"
            "/etc/asterisk/sip.conf"
            "/etc/audit/snare.conf"
            "/etc/crypttab"
            "/etc/filebeat/filebeat.yml"
            "/etc/fstab"
            "/etc/hosts"
            "/etc/httpd/conf.d/passenger.adk.conf"
            "/etc/httpd/conf.d/passenger.oamp-gui.conf"
            "/etc/httpd/conf.d/passenger.v2t_transcribe.conf"
            "/etc/init/mdd.conf"
            "/etc/nfsmount.conf"
            "/etc/ntp.conf"
            "/etc/ntp/step-tickers"
            "/etc/postfix/main.cf"
            "/etc/resolv.conf"
            "/etc/ssh/sshd_config"
            "/etc/sysconfig/nfs"
            "/opt/mcp/adk-services/app/controllers/sms_controller.rb"
            "/opt/mcp/adk-services/app/helpers/ami.rb"
            "/opt/mcp/adk-services/app/helpers/application_helper.rb"
            "/opt/mcp/adk-services/config/database.yml"
            "/opt/mcp/adk-services/config/environments/production.rb"
            "/opt/mcp/config/adk.yml"
            "/opt/mcp/config/database.yml"
            "/opt/mcp/config/dbconfig"
            "/opt/mcp/config/ha.yml"
            "/opt/mcp/config/pg_auth"
            "/opt/mcp/config/pgbouncer.ini"
            "/opt/mcp/config/scheduler.yml"
            "/opt/mcp/config/sms_gateway.yml"
            "/opt/mcp/courier/conf/mmail.conf"
            "/opt/mcp/cryptd/cryptd"
            "/opt/mcp/mdd/message_sender.rb"
            "/opt/mcp/mdd/user_export.rb"
            "/opt/mcp/model/config/database.yml"
            "/opt/mcp/oamp-gui/config/database.yml"
            "/opt/mcp/oamp-gui/config/mcp.yml"
            "/opt/mcp/sms_gateway/config/database.yml"
            "/opt/mcp/sms_receiver/config/smsMcpKannel.conf"
            "/opt/mcp/spm/.sysinfo"
            "/opt/mcp/v2t_transcribe/config/database.yml"
            "/usr/local/share/snmp/snmpd.conf"
            "/usr/local/share/snmp/snmpd.default.conf"
            "/usr/local/share/snmp/snmpd.movius.conf"
            "/opt/mcp/oamp-gui/config/passenger.oamp-gui.conf"

        )
    }

    Menus = @(
        @{
            text = 
"
--------------------------------------------------
 0. Main Menu
--------------------------------------------------
What would you like to do?
 1. Add Server
 2. List Servers
 9. Exit
Select"            
        }

    )
}