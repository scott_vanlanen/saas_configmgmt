#!/usr/bin/expect -f
################################################################################
# Common Expect Library
#   Author: SWV
#
################################################################################
#exp_internal 1

#-------------------------------------------------------------------------------
# Namespace
#-------------------------------------------------------------------------------
namespace eval ::msp::ssh:: {
  #variable prompt {\[\S*@\S*( |:)\S*(\] |\]# |\]\$ )}
  variable prompt {\[(\S*)@(\S*)[ :](\S*)\][#$]? }
  #variable var_passphrase ""

  namespace eval key {}
}

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
proc ::msp::ssh::key::getPassphrase {ll user_var} {
  log_user ${msp::log::verbose}

  upvar $user_var user_arr

  msp::log::sendUser $ll "PassPhrase Key for: ${user_arr(key_pri)}...\r\n"
  if {${msp::log::verbose} == 1} {send_user "\r\n"}
  spawn ssh-keygen -y -f ${user_arr(key_pri)}
  expect {
    timeout {
      close
    }
    -re {Enter passphrase.*} {
      stty -echo
      expect_user -timeout 3600 -re "(.*)\[\r\n]"
      stty echo
      set user_arr(key_phrase) $expect_out(1,string)
      send "${user_arr(key_phrase)}\r"
      exp_continue
    }
    -re {.*incorrect passphrase.*} {
      msp::log::sendUser $ll "\r\nIncorrect Passphrase .............\r\n"
      close
      exit
    }
    -re {.*No such file or directory.*} {
      msp::log::sendUser $ll "\r\nInvaild Key File .............\r\n"
      close
      exit
    }
    eof {
    }
  }
  wait

}

proc ::msp::ssh::key::clear {ll ip} {
  log_user ${msp::log::verbose}

  msp::log::sendUser $ll "Clear Key for: $ip...\r\n"
  if {${msp::log::verbose} == 1} {send_user "\r\n"}
  spawn ssh-keygen -R $ip
  expect {
    timeout {
      close
    }
    eof {
    }
  }
  wait

}

proc ::msp::ssh::key::generate {ll pass_phrase file_name} {
  log_user ${msp::log::verbose}

  msp::log::sendUser $ll "Generate Key: $file_name...\r\n"
  if {${msp::log::verbose} == 1} {send_user "\r\n"}
  spawn ssh-keygen -N $pass_phrase -f $file_name
  expect {
    timeout {
      close
    }
    eof {
    }
  }
  wait

}

proc ::msp::ssh::login {ll server_ip user_login host_pass timeout_min delay_sec} {
  log_user ${msp::log::verbose}

  set timeout $delay_sec
  set start [clock seconds]
  set now [clock seconds]
  set timeout_cnt [expr $timeout_min*60]

  set logged_in "no"
  set no_route 1
  set refused  1

  msp::log::sendUser $ll "Connecting ssh $user_login@$server_ip..."
  incr ll

  while {[expr {$now - $start}] < $timeout_cnt && $logged_in == "no"} {

    if {${msp::log::verbose} == 1} {send_user "\r\n"}
    spawn ssh $user_login@$server_ip
    expect {
      "Are you sure you want to continue connecting (yes/no)? " {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nAdding RSA key ....... $diff_str"
        send "yes\r"
	exp_continue
      }
      -re {.*Permission denied.*\n} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nPermission denied ... $diff_str"
        set logged_in "error"
        close
        wait
      }
      -re {.*No route to host.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$no_route} {
          msp::log::sendUser $ll "\r\nNo route to host ... $diff_str"
          set no_route 0
        } else {
          msp::log::sendUser $ll "\rNo route to host ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {.*Connection refused.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$refused} {
          msp::log::sendUser $ll "\r\nConnection refused ... $diff_str"
          set refused 0
        } else {
          msp::log::sendUser $ll "\rConnection refused ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {[P|p]assword: } {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        send "$host_pass\r"
        msp::log::sendUser $ll "\r\nEntering Password .... $diff_str"
        exp_continue
      }
      -re ${msp::ssh::prompt} {
        set logged_in "yes"
      }
      timeout {
        close
        wait
      }
      eof {
        wait
      }
    }
  }

  if {$logged_in == "yes"} {
    set now [clock seconds]
    set diff [expr {$now - $start}]
    set diff_str [msp::common::formatTime $diff]        
    msp::log::sendUser $ll "\r\nConneted ............. ${diff_str}\r\n"
    return $spawn_id
  } else {
    exit
  }

}

proc ::msp::ssh::userLogin {ll server_ip user_login timeout_min delay_sec} {
  log_user ${msp::log::verbose}

  set timeout $delay_sec
  set start [clock seconds]
  set now [clock seconds]
  set timeout_cnt [expr $timeout_min*60]

  set logged_in "no"
  set no_route 1
  set refused  1

  msp::log::sendUser $ll "Connecting ssh $user_login@$server_ip..."
  incr ll

  while {[expr {$now - $start}] < $timeout_cnt && $logged_in == "no"} {

    if {${msp::log::verbose} == 1} {send_user "\r\n"}
    spawn ssh $user_login@$server_ip
    expect {
      "Are you sure you want to continue connecting (yes/no)? " {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nAdding RSA key ....... $diff_str"
        send "yes\r"
	exp_continue
      }
      -re {.*Permission denied.*\n} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nPermission denied ... $diff_str"
        set logged_in "error"
        close
        wait
      }
      -re {.*No route to host.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$no_route} {
          msp::log::sendUser $ll "\r\nNo route to host ... $diff_str"
          set no_route 0
        } else {
          msp::log::sendUser $ll "\rNo route to host ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {.*Connection refused.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$refused} {
          msp::log::sendUser $ll "\r\nConnection refused ... $diff_str"
          set refused 0
        } else {
          msp::log::sendUser $ll "\rConnection refused ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {[P|p]assword: } {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        #send "$host_pass\r"
        #msp::log::sendUser $ll "\r\nEntering Password .... $diff_str"
        #exp_continue
        stty -echo
        expect_user -timeout 3600 -re "(.*)\[\r\n]"
        stty echo
        #set ::cfg::sys::user_key_phrase $expect_out(1,string)
        send "$expect_out(1,string)\r"
        exp_continue
        }
      -re ${msp::ssh::prompt} {
        set logged_in "yes"
      }
      timeout {
        close
        wait
      }
      eof {
        wait
      }
    }
  }

  if {$logged_in == "yes"} {
    set now [clock seconds]
    set diff [expr {$now - $start}]
    set diff_str [msp::common::formatTime $diff]        
    msp::log::sendUser $ll "\r\nConneted ............. ${diff_str}\r\n"
    return $spawn_id
  } else {
    exit
  }

}

proc ::msp::ssh::key::login {ll server_ip user_var timeout_min delay_sec} {
  log_user ${msp::log::verbose}

  upvar $user_var user_arr

  set timeout_cnt [expr $timeout_min*60]
  set timeout $delay_sec
  set start [clock seconds]
  set now [clock seconds]

  set logged_in "no"
  set no_route 1
  set refused  1

  msp::log::sendUser $ll "Connecting ssh ${user_arr(name)}@$server_ip..."
  incr ll

  while {[expr {$now - $start}] < $timeout_cnt && $logged_in == "no"} {

    if {${msp::log::verbose} == 1} {send_user "\r\n"}
    spawn ssh -i ${user_arr(key_pri)} ${user_arr(name)}@$server_ip
    expect {
      "Are you sure you want to continue connecting (yes/no)? " {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nAdding RSA key ....... $diff_str"
        send "yes\r"
	exp_continue
      }
      -re {.*Permission denied.*\n} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nPermission denied ... $diff_str"
        set logged_in "error"
        close
        wait
      }
      -re {.*No route to host.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$no_route} {
          msp::log::sendUser $ll "\r\nNo route to host ... $diff_str"
          set no_route 0
        } else {
          msp::log::sendUser $ll "\rNo route to host ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {.*Connection refused.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$refused} {
          msp::log::sendUser $ll "\r\nConnection refused ... $diff_str"
          set refused 0
        } else {
          msp::log::sendUser $ll "\rConnection refused ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {Enter passphrase for key.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        send "${user_arr(key_phrase)}\r"
        msp::log::sendUser $ll "\r\nEntering Passphrase .... $diff_str"
        exp_continue
      }
      -re {[P|p]assword: } {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nKey Failed .... $diff_str"
        set logged_in "error"
        close
        wait
      }
      -re ${msp::ssh::prompt} {
        set logged_in "yes"
      }
      timeout {
        close
        wait
      }
      eof {
        wait
      }
    }
  }

  if {$logged_in == "yes"} {
    set now [clock seconds]
    set diff [expr {$now - $start}]
    set diff_str [msp::common::formatTime $diff]        
    msp::log::sendUser $ll "\r\nConneted ............. ${diff_str}\r\n"
    return $spawn_id
  } else {
    exit
  }

}

proc ::msp::ssh::key::chkLogin {ll server_ip user_var timeout_min delay_sec} {
  log_user ${msp::log::verbose}

  upvar $user_var user_arr

  set timeout_cnt [expr $timeout_min*60]
  set timeout $delay_sec
  set start [clock seconds]
  set now [clock seconds]

  set logged_in "no"
  set no_route 1
  set refused  1
  set result   0

  msp::log::sendUser $ll "Check connecting ssh ${user_arr(name)}@$server_ip..."
  incr ll

  while {[expr {$now - $start}] < $timeout_cnt && $logged_in == "no"} {

    if {${msp::log::verbose} == 1} {send_user "\r\n"}
    spawn ssh -i ${user_arr(key_pri)} ${user_arr(name)}@$server_ip
    expect {
      "Are you sure you want to continue connecting (yes/no)? " {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nAdding RSA key ....... $diff_str"
        send "yes\r"
	exp_continue
      }
      -re {.*Permission denied.*\n} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nPermission denied ... $diff_str"
        set logged_in "error"
        close
        wait
      }
      -re {.*No route to host.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$no_route} {
          msp::log::sendUser $ll "\r\nNo route to host ... $diff_str"
          set no_route 0
        } else {
          msp::log::sendUser $ll "\rNo route to host ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {.*Connection refused.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        if {$refused} {
          msp::log::sendUser $ll "\r\nConnection refused ... $diff_str"
          set refused 0
        } else {
          msp::log::sendUser $ll "\rConnection refused ... $diff_str"
        }
        close
        wait
        sleep 1
      }
      -re {Enter passphrase for key.*} {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        send "${user_arr(key_phrase)}\r"
        msp::log::sendUser $ll "\r\nEntering Passphrase .... $diff_str"
        exp_continue
      }
      -re {[P|p]assword: } {
        set now [clock seconds]
        set diff [expr {$now - $start}]
        set diff_str [msp::common::formatTime $diff]        
        msp::log::sendUser $ll "\r\nKey Failed .... $diff_str"
        set logged_in "error"
        close
        wait
      }
      -re ${msp::ssh::prompt} {
        set logged_in "yes"
      }
      timeout {
        close
        wait
      }
      eof {
        wait
      }
    }
  }

  if {$logged_in == "yes"} {
    set now [clock seconds]
    set diff [expr {$now - $start}]
    set diff_str [msp::common::formatTime $diff]        
    msp::log::sendUser $ll "\r\nConneted ............. ${diff_str}\r\n"
    close
    set result 1
  } else {
    #exit
  }

  return $result
}

proc ::msp::ssh::setup {ll ssh_id} {
  log_user ${msp::log::verbose}

  set file_path "/etc/ssh/sshd_config"
  set sed_list [list \
    "Change GSSAPIAuthentication yes->no..." \
      "/^GSSAPIAuthentication yes/!d" \
      "s/^GSSAPIAuthentication yes/GSSAPIAuthentication no/g" \
    "Change GSSAPICleanupCredentials->#...." \
      "/^GSSAPICleanupCredentials yes/!d" \
      "s/^GSSAPICleanupCredentials yes/#GSSAPICleanupCredentials yes/g" \
    "Change UseDNS yes->no................." \
      "/^#UseDNS yes/!d" \
      "s/^#UseDNS yes/UseDNS no/g" \
  ]
    #"Changing PasswordAuthentication....." "'s/^PasswordAuthentication yes/PasswordAuthentication no/g'" \
    #"Changing PermitRootLogin............" "'s/^#PermitRootLogin yes/PermitRootLogin no/g'" \

  msp::log::sendUser $ll "Setup sshd config...\r\n" $ssh_id
  incr ll

  set sed_list [msp::common::sedCheckList $ll $ssh_id $file_path $sed_list]
  if {[llength $sed_list]!="0"} {
    msp::common::sedFileList $ll $ssh_id $file_path $sed_list {flags "-i"}
    #msp::cafeInstallMop::serviceRestart $ll $ssh_id "sshd"
    msp::linux::serviceRestart $ll $ssh_id "sshd"
  }
}

proc ::msp::ssh::noRoot {ll ssh_id} {
  log_user ${msp::log::verbose}

  set file_path "/etc/ssh/sshd_config"
  set sed_list [list \
    "Change PermitRootLogin................" \
      "/^#PermitRootLogin yes/!d" \
      "s/^#PermitRootLogin yes/PermitRootLogin no/g" \
  ]
    #"Changing PasswordAuthentication....." "'s/^PasswordAuthentication yes/PasswordAuthentication no/g'" \

  msp::log::sendUser $ll "Setup sshd config...\r\n" $ssh_id
  incr ll

  set sed_list [msp::common::sedCheckList $ll $ssh_id $file_path $sed_list]
  if {[llength $sed_list]!="0"} {
    msp::common::sedFileList $ll $ssh_id $file_path $sed_list {flags "-i"}
    #msp::cafeInstallMop::serviceRestart $ll $ssh_id "sshd"
    msp::linux::serviceRestart $ll $ssh_id "sshd"
  }
}


