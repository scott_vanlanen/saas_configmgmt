#!/usr/bin/expect -f
################################################################################
# Git Expect Library
#   Author: SWV
#
################################################################################
#exp_internal 1

#-------------------------------------------------------------------------------
# Namespace
#-------------------------------------------------------------------------------
namespace eval ::msp::git:: {
}

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
proc ::msp::common::checkArgs {script number string args} {

  #send_user "$args\r\n"
  #send_user "[llength $args]\r\n"

  # Check arguments
  if {[llength $args] ne $number} {
    send_user "Usage : ${script} ${string}\r\n"
    exit
  }
}

proc ::msp::common::cmdExit {ll ssh_id exit_script} {
  log_user ${msp::log::verbose}

  set timeout 3
  set spawn_id $ssh_id

    catch {
      msp::log::sendUser $ll "Exit...\r\n" $spawn_id
      send "exit\r"
      expect {
        -re {login: } {
          send \003
          expect {
            -re ${msp::ssh::prompt} {
              if {$exit_script == "true"} {
                send "exit\r"
                exp_continue
              }
            }
          }
        }
        eof {
          wait
        }
        timeout {
          send \003
          close
          wait
          #send_user -- "------------------------END--------------------------\r\n"
        }
        #-re ${MutiPrompt} {
        #}
      }
    } err
  
    if {$exit_script == "true"} {
      send_user -- "------------------------END SCRIPT--------------------------\r\n"
      exit
    }
    #msp::log::sendUser "\r\n" 2

}

proc ::msp::common::formatTime {time_sec} {

  set secs [expr { int(floor($time_sec)) % 60 }]
  set hrs  [expr { int(floor($time_sec / 3600)) }]
  set mins [expr { int(floor($time_sec / 60)) % 60 }]
  return [format "%02d:%02d:%02d" $hrs $mins $secs]

}

proc ::msp::common::maxLengthList {list args} {

  if {[llength $args] == 1} {
    array set opts [lindex $args 0]
  } else {
    array set opts {}
  }

  set max_legth 0

  foreach item $list {
    if {([info exists opts(split)] && [info exists opts(index)])} {
      set item_vals [split $item $opts(split)]
      set item_val  [lindex $item_vals $opts(index)]
      if {[string length $item_val]>$max_legth} {
        set max_legth [string length $item_val]
      }
    } else {
      if {[string length $item]>$max_legth} {
        set max_legth [string length $item]
      }
    }
  }
  
  return $max_legth

}

proc ::msp::common::checkDir {ll ssh_id dir_path} {
  log_user ${msp::log::verbose}

  set spawn_id $ssh_id
  set result 0

  msp::log::sendUser $ll "Does Directory '$dir_path' Exist..." $ssh_id

  send "test -d '$dir_path' && echo 'yes'\r"
  expect {
    timeout {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "Permission denied" {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "\r\nyes" {
      set result 1
      exp_continue
    }
    -re ${msp::ssh::prompt} {
    }
  }

  if {$result == 1} {
    msp::log::sendUser $ll " True\r\n"
  } else {
    msp::log::sendUser $ll " False\r\n"
  }

  return $result

}

proc ::msp::common::createDir {ll ssh_id dir_path} {
  log_user ${msp::log::verbose}

  set spawn_id $ssh_id

  msp::log::sendUser $ll "Creating Directory: '$dir_path'...\r\n" $ssh_id

  send "mkdir '$dir_path'\r"
  expect {
    "mkdir: can't create directory*] " {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    timeout {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    -re ${msp::ssh::prompt} {
    }
  }

}

proc ::msp::common::checkFile {ll ssh_id file_path} {
  log_user ${msp::log::verbose}

  set spawn_id $ssh_id
  set result 0

  msp::log::sendUser $ll "Does File '$file_path' Exist..." $ssh_id

  send "test -f '$file_path' && echo 'yes'\r"
  expect {
    timeout {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "\r\nyes" {
      set result 1
      exp_continue
    }
    -re ${msp::ssh::prompt} {
    }
  }

  if {$result == 1} {
    msp::log::sendUser $ll " True\r\n"
  } else {
    msp::log::sendUser $ll " False\r\n"
  }

  return $result

}

proc ::msp::common::cpFile {ll ssh_id file_path new_path args} {
  log_user ${msp::log::verbose}
  set timeout -1

  set spawn_id $ssh_id
  set cmd ""

  if {[llength $args] == 1} {
    array set opts [lindex $args 0]
  } else {
    array set opts {}
  }

  if {[info exists opts(prefix)]} {
    append cmd $opts(prefix)
    #append cmd " "
  }
  append cmd "cp "
  if {[info exists opts(flags)]} {
    append cmd $opts(flags)
    append cmd " "
  }
  append cmd "'$file_path' '$new_path'"

  send "$cmd\r"
  expect {
    timeout {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "cp: overwrite" {
      send "n\r"
      expect -re ${msp::ssh::prompt} {
        msp::common::cmdExit $ll $spawn_id "true"
      }
    }
    "cp: cannot create regular file" {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "cp: cannot stat" {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "Permission denied" {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    -re ${msp::ssh::prompt} {
    }
  }

}

proc ::msp::common::sedCheckList {ll ssh_id file_path chk_lst args} {
  log_user ${msp::log::verbose}

  set spawn_id $ssh_id

  if {[llength $args] == 1} {
    array set opts [lindex $args 0]
  } else {
    array set opts {}
  }

  set i 0
  while {$i < [llength $chk_lst]} {
    set user_str [lindex $chk_lst $i]
    set chk_str  [lindex $chk_lst [expr $i+1]]
    set cmd_str  [lindex $chk_lst [expr $i+2]]

    set result "0"

    msp::log::sendUser $ll "Check '$user_str' in '$file_path'..." $ssh_id
    send "sed '$chk_str' '$file_path'\r"
    expect {
      "sed: *: No such file or directory" {
        set result "-1"
        exp_continue
      }
      "Permission denied" {
        set result "-1"
        exp_continue
      }
      timeout {
        set result "-1"
      }
      -re {\r\n(.*)\r\n} {
        set result "1"
        exp_continue
      }
      -re ${msp::ssh::prompt} {
      }
    }

    if {([info exists opts(invert)] && $opts(invert)=="Y")} {
      if {$result=="0"} {
        set result "1"
      } elseif {$result=="1"} {
        set result "0"
      }
    }

    switch $result {
      "-1" {
        msp::log::sendUser 1 " ERROR\r\n"
        msp::common::cmdExit $ll $spawn_id "true"
      }
      "0" {
        msp::log::sendUser 1 " False\r\n"
        set chk_lst [lreplace $chk_lst $i [expr $i+2]]
      }
      "1" {
        msp::log::sendUser 1 " True\r\n"
        set chk_lst [lreplace $chk_lst [expr $i+1] [expr $i+1]]
        incr i 2
      }
    }

  }

  return $chk_lst

}

proc ::msp::common::sedFileList {ll ssh_id file_path sed_lst args} {
  log_user ${msp::log::verbose}

  set spawn_id $ssh_id
  set cmd ""

  if {[llength $args] == 1} {
    array set opts [lindex $args 0]
  } else {
    array set opts {}
  }

  if {[info exists opts(prefix)]} {
    append cmd $opts(prefix)
    append cmd " "
  }
  append cmd "sed "
  if {[info exists opts(flags)]} {
    append cmd $opts(flags)
    append cmd " "
  }

  msp::log::sendUser $ll "Sed '$file_path'...\r\n"
  incr ll

  set i 0
  while {$i < [llength $sed_lst]} {
    set user_str [lindex $sed_lst $i]
    set cmd_str  [lindex $sed_lst [expr $i + 1]]

    msp::log::sendUser $ll "$user_str" $spawn_id
    send "$cmd'$cmd_str' '$file_path'\r"
    expect {
      "sed: *: No such file or directory" {
        -re ${msp::ssh::prompt} {
          msp::log::sendUser $ll " Error" $spawn_id
          msp::common::cmdExit $ll $spawn_id "true"
        }
      }
      "Permission denied" {
        msp::common::cmdExit $ll $spawn_id "true"
      }
      timeout {
        send_user "Error"
        msp::common::cmdExit $ll $spawn_id "true"
      }
      -re ${msp::ssh::prompt} {
        msp::log::sendUser $ll "\r$user_str Done\r\n"
      }
    }
    set i [expr $i + 2]
  }

}

proc ::msp::common::writeFileList {ll ssh_id file_path append_lst args} {
  log_user ${msp::log::verbose}

  set spawn_id $ssh_id
  set cmd ""

  if {[llength $args] == 1} {
    array set opts [lindex $args 0]
  } else {
    array set opts {}
  }
  if {[info exists opts(prefix)]} {
    append cmd $opts(prefix)
    append cmd " "
  }
  append cmd "tee -a"

  msp::log::sendUser $ll "Writing to '$file_path'...\r\n" $spawn_id
  incr ll

  set i 0
  while {$i < [llength $append_lst]} {
    #set user_str [lindex $append_lst $i]
    #set cmd_str  [lindex $append_lst [expr $i + 1]]
    set cmd_str  [lindex $append_lst $i]

    #msp::log::sendUser $ll "$user_str" $spawn_id
    send "echo '$cmd_str' | $cmd '$file_path'\r"
    expect {
      timeout {
        send_user "Error"
        msp::common::cmdExit $ll $spawn_id "true"
      }
      "Permission denied" {
        msp::common::cmdExit $ll $spawn_id "true"
      }
      -re ${msp::ssh::prompt} {
        #msp::log::sendUser $ll "\r$user_str Done\r\n"
        msp::log::sendUser $ll "\r$cmd_str\r\n"
      }
#      "\r\n$cmd_str\r\n" {
#        send_user "Done"
#        exp_continue
#      }
    }
    #set i [expr $i + 2]
    incr i
  }

}

proc ::msp::common::rmCmd {ll ssh_id path args} {
  log_user ${msp::log::verbose}
  set timeout -1

  set spawn_id $ssh_id
  set cmd ""

  if {[llength $args] == 1} {
    array set opts [lindex $args 0]
  } else {
    array set opts {}
  }

  if {[info exists opts(prefix)]} {
    append cmd $opts(prefix)
    append cmd " "
  }
  append cmd "rm "
  if {[info exists opts(flags)]} {
    append cmd $opts(flags)
    append cmd " "
  }
  append cmd "'$path'"

  msp::log::sendUser $ll "Delete $path...\r\n" $spawn_id

  send "$cmd\r"
  expect {
    timeout {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    "Permission denied" {
      msp::common::cmdExit $ll $spawn_id "true"
    }
    -re ${msp::ssh::prompt} {
    }
  }

}

