#!/usr/bin/expect -f
################################################################################
# Create HA System Expect Script
#   Author: SWV
#   Notes : It's Script can/will create many VMs and can take very long.
#         : Make sure you have a relible connection
#
################################################################################
#exp_internal 1

#-------------------------------------------------------------------------------
# Include Config
#-------------------------------------------------------------------------------
source [file join [file dirname [info script]] lib/common.tcl]
#source [file join [file dirname [info script]] lib/log.tcl]
#source [file join [file dirname [info script]] lib/local.tcl]
source [file join [file dirname [info script]] lib/ssh.tcl]
#source [file join [file dirname [info script]] lib/admin.tcl]
#source [file join [file dirname [info script]] lib/vmware.tcl]
#source [file join [file dirname [info script]] lib/linux.tcl]
#source [file join [file dirname [info script]] lib/cafe.tcl]
#source [file join [file dirname [info script]] lib/license.tcl]

#-------------------------------------------------------------------------------
# Main Script
#-------------------------------------------------------------------------------
proc main {script args} {

  # TO-DO: Better checking of config file
#  msp::common::checkArgs $script 1 "<config_file>" {*}$args
#  source [file join [file dirname [info script]] [lindex $args 0]]

  set scr_str [clock seconds]
  set ll 1
  
# Get Passphrase for SSH Key
#  set    user_var "::cfg::user"
#  upvar $user_var user_arr
#  send_user -- "----------------------------------------------------------\r\n"
#  send_user -- " Get SSH Key Passphrase: ${user_arr(key_pri)}\r\n"
#  send_user -- "----------------------------------------------------------\r\n"
#  msp::ssh::key::getPassphrase $ll $user_var

  # Setup ALL VMware VMs (Kickstart and Drive setup)
#  if {1} {
#    msp::vmware::setupVms $ll "::cfg::vms"
#  }
#
#  # Setup Linux of ALL VMs
#  if {1} {
#    msp::linux::setup $ll "::cfg::vms"
#  }
#
#  # Install VMware tools for ALL VMs
#  if {1} {
#    msp::vmware::tools::install $ll "::cfg::vms"
#  }
#
#  # Add Nodes
#  if {1} {
#    msp::cafe::addNodes $ll "::cfg::vms"
#  }

  set scr_end [clock seconds]
  set diff [expr {$scr_end - $scr_str}]
  set diff_str [msp::common::formatTime $diff]

  send_user -- "------------------------Time: $diff_str--------------------------\r\n"
  send_user -- "---------------------------END GOOD-----------------------------\r\n"

  exit

}

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Start the Script
#-------------------------------------------------------------------------------
main ${argv0} {*}$argv


