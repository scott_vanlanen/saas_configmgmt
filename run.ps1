################################################################################
# Main Run Menu Script for SaaS Server Config Management
#   Author: SWV
#   Notes : 
#         : 
#
################################################################################
$FormatServer = @{Label="ID";         Width=3;  Expression="ID"}, `
                @{Label="Enviroment"; Width=24; Expression="Enviroment"}, `
                @{Label="Server";     Width=16; Expression="Server"}

#-------------------------------------------------------------------------------
# Menus
#-------------------------------------------------------------------------------
Function Main {
    Set-Location $PSScriptRoot

    #write-host ("Load Config $PSScriptRoot\ConfigMgmt.psd1...")
    $ConfigMgmt = Import-PowerShellDataFile $PSScriptRoot\ConfigMgmt.psd1
    #write-host ("...Config Loaded " + $ConfigMgmt.Servers.Count + " Servers")

    $Cred = Get-Credential
    
    Do {
        Set-Location $PSScriptRoot
    
        write-host "--------------------------------------------------------------------------------"
        write-host " 0. Main Menu"
        write-host "--------------------------------------------------------------------------------"
        write-host "1. Load Server"
        write-host "2. Get Servers"
        write-host "3. Add Server"
        write-host "4. Remove Server"
        write-host "5. Sync Server"
        write-host "6. List Servers"
        write-host "0. Exit"
        #$Menu_Level = 0
        #While ($user_input = (Read-Host -Prompt $ConfigMgmt.Menus[$Menu_Level].text)) {
    
        $user_input = (Read-Host -Prompt "Select a Command or Press ENTER to Refresh")
        Switch ($user_input)
        {
            "1" {MnuLoadServer $ConfigMgmt $Cred "$PSScriptRoot\ServerMgmt.json"; break}
            "2" {MnuGetServer; break}
            "3" {MnuAddServer $ConfigMgmt $Cred; break}
            "4" {MnuRemoveServer $ConfigMgmt; break}
            "5" {MnuSyncServers $ConfigMgmt; break}
            "6" {MnuListServers $ConfigMgmt; break}
            "7" {testNew; break}
        }
        
    } While ($user_input -NE "0")
}
Function testNew {
    write-host "--- Start ---"

    write-host "--- Done ---"
}
Function MnuListServers {
    param(
        [parameter(Mandatory)][PSObject]$ConfigMgmt
    )

    Do {
        write-host ("--------------------------------------------------------------------------------")
        write-host (" 1. List Servers")
        write-host ("--------------------------------------------------------------------------------")

        [array]$GitWorkTree = GitWorkTree
        [array]$GitRemoteBranchs = GitRemoteBranchs

        ForEach ($GWTree in $GitWorkTree) {
            ForEach ($GRBranch in $GitRemoteBranchs) {
                if ($GRBranch.Remote_Branch -eq ("origin/" + $GWTree.Local_Branch.Trim("[", "]"))) {
                    $GWTree.Remote_Branch = $GRBranch.Remote_Branch
                    $GRBranch.Remote_Branch = $null
                } 
            }
        }

        $GitWorkTree += $GitRemoteBranchs | Where-Object {$_.Remote_Branch}

        $GitWorkTree | Format-Table `
                     | Out-String `
                     | ForEach-Object Trim `
                     | Write-Host

        write-host ("--------------------------------------------------------------------------------")
        write-host ("0. Back")

        $user_input = (Read-Host -Prompt "Select a Command or Press ENTER to Refresh")
    } While ($user_input -ne "0")
}   
Function MnuGetServer {
    Do {
        write-host ("--------------------------------------------------------------------------------")
        write-host (" 2. Get Server")
        write-host ("--------------------------------------------------------------------------------")
    
        [array]$GitWorkTree = GitWorkTree `
                            | Select-Object -Property ID, Path, Local_Branch, Remote_Branch `
                            | Where-Object Local_Branch -NE "[master]"

        [array]$GitRemoteBranchs = GitRemoteBranchs `
                                 | Where-Object Remote_Branch -NE "origin/master"

        foreach ($GWTree in $GitWorkTree) {
            foreach ($GRBranch in $GitRemoteBranchs) {
                if ($GRBranch.Remote_Branch -eq ("origin/" + $GWTree.Local_Branch.Trim("[", "]"))) {
                    $GWTree.Remote_Branch = $GRBranch.Remote_Branch
                    $GRBranch.Remote_Branch = $null
                } 
            }
        }
        
        $GitWorkTree += $GitRemoteBranchs | Where-Object Remote_Branch
    
        $i = 1
        $GitWorkTree | ForEach-Object {$_.ID = $i++}
    
        $GitWorkTree | Format-Table `
                     | Out-String -Stream `
                     | Where-Object Length -NE 0 `
                     | ForEach-Object Insert 0 "  " `
                     | Write-Host
    
        #write-host "--------------------------------------------------------------------------------"
        write-host ""
    
        [string]$user_input = (Read-Host -Prompt "Select ID to ADD or Press 0 to Exit")
    
        $AddBranch = $GitWorkTree | Where-Object ID -EQ $user_input
    
        if ($AddBranch) {
            $AddBranch = $AddBranch.Remote_Branch -replace "origin/"
            git worktree add --checkout ../$AddBranch $AddBranch
        }

    } While ($user_input -NE "0")

    return $Result
}
Function MnuAddServer {
    param(
        [parameter(Mandatory)][PSObject]$ConfigMgmt,
        [parameter(Mandatory)][PSCredential]$Cred
    )

    Do {
        Set-Location $PSScriptRoot
        write-host "--------------------------------------------------------------------------------"
        write-host " 3. Add Server"
        write-host "--------------------------------------------------------------------------------"
        write-host ""
        
        $Server = Read-Host -Prompt "Enter Server(ip or name)"
        AddServer $ConfigMgmt $Cred $Server

        [string]$user_input = (Read-Host -Prompt "Press 0 to Exit")
    } While ($user_input -NE "0")
}
Function MnuRemoveServer {
    Do {
        write-host ("--------------------------------------------------------------------------------")
        write-host (" 1. Remove Server Configuration Branch")
        write-host ("--------------------------------------------------------------------------------")
        write-host ("")

        [array]$GitWorkTree = GitWorkTree `
                            | Select-Object -Property ID, Path, Local_Branch, Remote_Branch `
                            | Where-Object Local_Branch -NE "[master]"

        [array]$GitRemoteBranchs = GitRemoteBranchs `
                                 | Select-Object -Property ID, Path, Local_Branch, Remote_Branch `
                                 | Where-Object Remote_Branch -NE "origin/master"

        ForEach ($GWTree in $GitWorkTree) {
            ForEach ($GRBranch in $GitRemoteBranchs) {
                if ($GRBranch.Remote_Branch -eq ("origin/" + $GWTree.Local_Branch.Trim("[", "]"))) {
                    $GWTree.Remote_Branch = $GRBranch.Remote_Branch
                    $GRBranch.Remote_Branch = $null
                } 
            }
        }

        $GitWorkTree += $GitRemoteBranchs | Where-Object Remote_Branch
    
        $i = 1
        $GitWorkTree | ForEach-Object {$_.ID = $i++}
    
        $GitWorkTree | Format-Table `
                     | Out-String -Stream `
                     | Where-Object Length -NE 0 `
                     | ForEach-Object Insert 0 "  " `
                     | Write-Host
    
        #write-host "--------------------------------------------------------------------------------"
        write-host ""
    
        [string]$user_input = (Read-Host -Prompt "Select ID to REMOVE or Press 0 to Exit")
    
        $RemoveBranch = $GitWorkTree | Where-Object {$_.ID -eq $user_input}
    
        if ($RemoveBranch) {
            if ($RemoveBranch.Local_Branch) {
                $RemoveBranch = $RemoveBranch.Local_Branch.Trim("[", "]")
            
                write-host $RemoveBranch
                #delete the folder of the worktree
                Remove-Item ..\$RemoveBranch -Recurse -Force
            
                # delete the worktree itself
                git worktree prune
            
                # To delete a local branch
                git branch -D $RemoveBranch
            } elseif ($RemoveBranch.Remote_Branch) {
                $RemoveBranch = $RemoveBranch.Remote_Branch -replace "origin/"
                [string]$user_input = (Read-Host -Prompt 'Are You Sure? Enter "DELETE" to delete remote Branch')
                write-host "$user_input : $RemoveBranch"
                if ($user_input -EQ "DELETE") {
                    git push origin --delete $RemoveBranch --porcelain
                }
            }
        }
    
    } While ($user_input -NE "0")

    return $Result
}
Function MnuSyncServers {
    param(
        [parameter(Mandatory)][PSCredential]$Credential
    )
    
    return $Result
}
Function MnuLoadServer {
    param(
        [parameter(Mandatory)][PSObject]$ConfigMgmt,
        [parameter(Mandatory)][PSCredential]$Cred,
        [parameter(Mandatory)][PSObject]$path
    )

    Do {
        Set-Location $PSScriptRoot
    
        write-host "--------------------------------------------------------------------------------"
        write-host " 6. Load Enviroment From File"
        write-host "--------------------------------------------------------------------------------"

        if (Test-Path $path -PathType Leaf) {
            $jsonFileData = Get-Content $path
            $servers = $jsonFileData | ConvertFrom-Json
        }

        if ($null -ne $servers.Enviroments) {
            $i = 1
            $enviroments = $servers.Enviroments `
                         | Select-Object -Property ID, ServerCnt, Name, Servers

            $enviroments | ForEach-Object {$_.ID = $i++; $_.ServerCnt = "$($_.Servers.Count)"}
        
            $enviroments | Select-Object -Property ID, Name, ServerCnt `
                         | Format-Table `
                         | Out-String -Stream `
                         | Where-Object Length -NE 0 `
                         | ForEach-Object Insert 0 "  " `
                         | Write-Host

            Write-Host ""
        } else {
            write-host "ERROR"
        }

        [string]$user_input = (Read-Host -Prompt "Select ID to Load or Press 0 to Exit")

        write-host ("--------------------------------------------------------------------------------")
    
        $enviroment = $enviroments | Where-Object ID -EQ $user_input
    
        if ($null -ne $enviroment.Servers) {
            $enviroment.Servers | Format-Table `
                                | Out-String -Stream `
                                | Where-Object Length -NE 0 `
                                | ForEach-Object Insert 0 "  " `
                                | Write-Host

            $enviroment.Servers | ForEach-Object {AddServer $ConfigMgmt $Cred $_.IP}
            #Start-Sleep -Seconds 3

            <#
            $enviroment.Servers | Format-Table `
                                | Out-String -Stream `
                                | Where-Object Length -NE 0 `
                                | ForEach-Object Insert 0 "  " `
                                | Write-Host
            #>
        }

    } While ($user_input -NE "0")
}

#-------------------------------------------------------------------------------
# Fumctions
#-------------------------------------------------------------------------------
Function GitWorkTree {
    $strGitWorkTree = git worktree list
    
    [array]$GitWorkTree = $strGitWorkTree -Replace "[ ]{1,}", "," `
                        | ConvertFrom-Csv -Header Path, Hash, Local_Branch `
                        | Select-Object -Property Path, Local_Branch, Remote_Branch
    
    return $GitWorkTree
}
Function GitRemoteBranchs {
    $strGitRemoteBranchs = git branch -r

    [array]$GitRemoteBranchs = $strGitRemoteBranchs -Replace "[ ]{1,}", "," `
                             | ConvertFrom-Csv -Header blank, Remote_Branch `
                             | Select-Object -Property Path, Local_Branch, Remote_Branch `
                             | Where-Object {$_.Remote_Branch -ne "origin/HEAD"}

    return $GitRemoteBranchs
}

Function AddServer {
    param(
        [parameter(Mandatory)][PSObject]$ConfigMgmt,
        [parameter(Mandatory)][PSCredential]$Cred,
        [parameter(Mandatory)][PSObject]$Server
    )

    Set-Location $PSScriptRoot
    write-host "Server: $Server"
    write-host "Login_Name: $Cred.UserName"
    
    $SSH = New-SSHSession $Server -Credential $Cred
    
    # Get Results of 'hostname' Command
    $Results = Invoke-SSHCommand -Command 'hostname' -SessionId $SSH.SessionId -TimeOut 10

    if ($Results)
    {
        $ServerName = $Results.Output.GetValue(0)
        write-host "-------------"
        git worktree add -b $ServerName ../$ServerName
        Set-Location ../$ServerName
        [Void]$(git rm *)
        foreach ($file in $ConfigMgmt.CafeMas.Files) {
            if (!(Test-Path "./files$file"))
            {
                [Void]$(New-Item "./files$file" -Force)
            }
            Get-SCPFile $Server $Cred -RemoteFile $file -LocalFile "./files$file"
        }
        [Void]$(git add *)
        $TimeNow = Get-Date
        $UtcTime = $TimeNow.ToUniversalTime().ToString("dddd MM/dd/yyyy HH:mm")
        write-host "--- Commit ---"
        git commit -m "Script Update $ServerName $UtcTime"
        write-host "--- Push ---"
        git push --set-upstream origin $ServerName
        # Not sure if this is needed
        Set-Location $PSScriptRoot
        write-host "--- Merge ---"
        git merge -s ours $ServerName
        write-host "--- Push ---"
        git push
        write-host "------------"
    }

    # Create Server Directory
    #New-Item -Path $BranchPath -Name $Results.Output.GetValue(0) -ItemType "directory"

    # Remove all SSH Sessions
    [Void]$(Get-SSHSession | Remove-SSHSession)
}


#-------------------------------------------------------------------------------
# Start Here
#-------------------------------------------------------------------------------
Main

